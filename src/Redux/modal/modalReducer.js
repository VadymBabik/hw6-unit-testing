import * as types from "./actions";

const initialState = {
  isModal: false,
  idModalProduct: null,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.MODAL_SHOW:
      return { ...state, isModal: !state.isModal };
    case types.MODAL_ID_PRODUCT:
      return { ...state, idModalProduct: action.payload };
    default:
      return state;
  }
};
