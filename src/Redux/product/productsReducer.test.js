import { productsReducer } from "./productsReducer";
import * as types from "./actions";

describe("Test for productsReducer", () => {
  it("LOADING_CARDS", () => {
    const stateBefore = {
      isLoading: false,
      isError: null,
      product: JSON.parse(localStorage.getItem("products")) || [],
    };
    const action = {
      type: types.LOADING_CARDS,
    };
    const reducer = productsReducer(stateBefore, action);
    expect(reducer.isLoading).toBeTruthy;
  });
  it("LOADING_ERROR", () => {
    const stateBefore = {
      isLoading: false,
      isError: null,
      product: JSON.parse(localStorage.getItem("products")) || [],
    };
    const action = {
      type: types.LOADING_ERROR,
      payload: "Error message",
    };
    expect(productsReducer(stateBefore, action)).toEqual({
      ...stateBefore,
      isError: action.payload,
      isLoading: false,
    });
  });
  it("FETCH_CARTS_SUCCESS", () => {
    const stateBefore = {
      isLoading: true,
      isError: "Error message",
      product: JSON.parse(localStorage.getItem("products")) || [],
    };
    const action = {
      type: types.FETCH_CARTS_SUCCESS,
      payload: [{ product: "product" }],
    };
    expect(productsReducer(stateBefore, action)).toEqual({
      ...stateBefore,
      isError: null,
      isLoading: false,
      product: [...stateBefore.product, ...action.payload],
    });
  });
  it("TOGGLE_FAVORITE", () => {
    const stateBefore = {
      product: [
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    };
    const action = {
      type: types.TOGGLE_FAVORITE,
      payload: 1,
    };
    const reducer = productsReducer(stateBefore, action);
    expect(reducer.product[0].isFavourite).toBeTruthy();
  });
  it("ADD_TO_CART", () => {
    const stateBefore = {
      isLoading: false,
      isError: null,
      product: [
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 2,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    };
    const action = {
      type: types.ADD_TO_CART,
      payload: 1,
    };
    expect(productsReducer(stateBefore, action)).toEqual({
      ...stateBefore,
      product: [
        {
          cart: { inCart: "true", count: 1 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 2,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    });
  });
  it("REMOVE_ONE_CART", () => {
    const stateBefore = {
      isLoading: false,
      isError: null,
      product: [
        {
          cart: { inCart: "true", count: 5 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 2,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    };
    const action = {
      type: types.REMOVE_ONE_CART,
      payload: 1,
    };
    expect(productsReducer(stateBefore, action)).toEqual({
      ...stateBefore,
      product: [
        {
          cart: { inCart: "true", count: 4 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 2,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    });
  });
  it("REMOVE_PRODUCT_CART", () => {
    const stateBefore = {
      isLoading: false,
      isError: null,
      product: [
        {
          cart: { inCart: "true", count: 5 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 2,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    };
    const action = {
      type: types.REMOVE_PRODUCT_CART,
      payload: 1,
    };
    expect(productsReducer(stateBefore, action)).toEqual({
      ...stateBefore,
      product: [
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
        {
          cart: { inCart: false, count: 0 },
          color: "Blue",
          id: 2,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    });
  });
  it("CLEAR_CART", () => {
    const stateBefore = {
      isLoading: false,
      isError: null,
      product: [
        {
          cart: { inCart: "true", count: 5 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
        {
          cart: { inCart: "true", count: 4 },
          color: "Blue",
          id: 2,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    };
    const action = {
      type: types.CLEAR_CART,
    };
    expect(productsReducer(stateBefore, action)).toEqual({
      ...stateBefore,
      product: [
        {
          cart: { inCart: "false", count: 0 },
          color: "Blue",
          id: 1,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
        {
          cart: { inCart: "false", count: 0 },
          color: "Blue",
          id: 2,
          imgUrl:
            "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
          isFavourite: false,
          price: 37000,
          productName: "Apple iPhone 12 Pro 128GB Pacific (MGMN3)",
          vendorСod: "675415",
        },
      ],
    });
  });
  it("undefined", () => {
    const stateBefore = {
      isLoading: false,
      isError: null,
      product: ["test"],
    };
    expect(productsReducer(stateBefore, {})).toEqual(stateBefore);
  });
});
