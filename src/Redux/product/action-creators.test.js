import * as types from "./actions";
import * as aCreator from "./action-creators";

describe("Test for action-creator productsReducer", () => {
  it("isLoading", () => {
    expect(aCreator.isLoading()).toEqual({
      type: types.LOADING_CARDS,
    });
  });
  it("isError", () => {
    expect(aCreator.isError("message")).toEqual({
      type: types.LOADING_ERROR,
      payload: "message",
    });
  });
  it("fetchCarts", () => {
    expect(aCreator.fetchCarts(["fetchCarts"])).toEqual({
      type: types.FETCH_CARTS_SUCCESS,
      payload: ["fetchCarts"],
    });
  });
  it("favorite", () => {
    expect(aCreator.favorite(1)).toEqual({
      type: types.TOGGLE_FAVORITE,
      payload: 1,
    });
  });
  it("addInCart", () => {
    expect(aCreator.addInCart(1)).toEqual({
      type: types.ADD_TO_CART,
      payload: 1,
    });
  });
  it("removeOneCart", () => {
    expect(aCreator.removeOneCart(1)).toEqual({
      type: types.REMOVE_ONE_CART,
      payload: 1,
    });
  });
  it("removeCart", () => {
    expect(aCreator.removeCart(1)).toEqual({
      type: types.REMOVE_PRODUCT_CART,
      payload: 1,
    });
  });
  it("clearCart", () => {
    expect(aCreator.clearCart(1)).toEqual({
      type: types.CLEAR_CART,
      payload: 1,
    });
  });
});
