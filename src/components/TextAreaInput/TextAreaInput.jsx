import {useField} from "formik";
import React from "react";

export const TextAreaInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
        <>
            <div className="input-field">
                <label htmlFor={props.id || props.name}>{label}</label>
                <textarea
                    className={`materialize-textarea ${
                        meta.touched && meta.error ? "invalid" : "validate"
                    }`}
                    {...field}
                    {...props}
                />
                <span
                    className="helper-text"
                    data-error={meta.error}
                    data-success="right"
                />
            </div>
        </>
    );
};
